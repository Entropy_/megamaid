// Copyright 2016 Arne Roomann-Kurrik
//
// Modifications to unlike the likes found
// Done by Entropy (py-entro@gitlab.com) 2019
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Downloads a user's favorites timeline and writes it to a file.
package main

// Reads as much of a user's public Likes (formerly Favorites) as it can
// and prints each Tweet to a file.
//
// This example respects rate limiting and will wait until the rate limit
// reset time to finish pulling a timeline.

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/kurrik/oauth1a"
	"github.com/kurrik/twittergo"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

func LoadCredentials() (client *twittergo.Client, err error) {
	credentials, err := ioutil.ReadFile("CREDENTIALS")
	if err != nil {
		return
	}
	lines := strings.Split(string(credentials), "\n")
	config := &oauth1a.ClientConfig{
		ConsumerKey:    lines[0],
		ConsumerSecret: lines[1],
	}
	user := oauth1a.NewAuthorizedConfig(lines[2], lines[3])
	client = twittergo.NewClient(config, user)
	return
}

type Args struct {
	ScreenName string
	OutputFile string
}

func parseArgs() *Args {
	a := &Args{}
	flag.StringVar(&a.ScreenName, "screen_name", "twitterapi", "Screen name")
	flag.StringVar(&a.OutputFile, "out", "favorites.json", "Output file")
	flag.Parse()
	return a
}

func main() {
  if len(os.Args) < 2 || len(os.Args) > 2 {
    fmt.Println("Usage is ./megamaid <USERNAME>")
    os.Exit(1)
  }
	var (
		err     error
		client  *twittergo.Client
		req     *http.Request
		resp    *twittergo.APIResponse
		args    *Args
		max_id  uint64
		out     *os.File
		query   url.Values
		results *twittergo.Timeline
		text    []byte
	)
	args = parseArgs()
	if client, err = LoadCredentials(); err != nil {
		fmt.Printf("Could not parse CREDENTIALS file: %v\n", err)
		os.Exit(1)
	}
	if out, err = os.Create(args.OutputFile); err != nil {
		fmt.Printf("Could not create output file: %v\n", args.OutputFile)
		os.Exit(1)
	}
	defer out.Close()
	const (
		count   int = 200
		urltmpl     = "/1.1/favorites/list.json?%v"
    urlUnlike   = "/1.1/favorites/destroy.json?%v"
		minwait     = time.Duration(10) * time.Second
	)
	query = url.Values{}
	query.Set("count", fmt.Sprintf("%v", count))
	query.Set("screen_name", os.Args[1])
	total := 0
	//Set an array for the tweet ids so we can unlike them
  var tweetIds []uint64


  for {
		if max_id != 0 {
			query.Set("max_id", fmt.Sprintf("%v", max_id))
		}
		endpoint := fmt.Sprintf(urltmpl, query.Encode())
		if req, err = http.NewRequest("GET", endpoint, nil); err != nil {
			fmt.Printf("Could not parse request: %v\n", err)
			os.Exit(1)
		}
		if resp, err = client.SendRequest(req); err != nil {
			fmt.Printf("Could not send request: %v\n", err)
			os.Exit(1)
		}
		results = &twittergo.Timeline{}
		if err = resp.Parse(results); err != nil {
			if rle, ok := err.(twittergo.RateLimitError); ok {
				dur := rle.Reset.Sub(time.Now()) + time.Second
				if dur < minwait {
					// Don't wait less than minwait.
					dur = minwait
				}
				msg := "Rate limited. Reset at %v. Waiting for %v\n"
				fmt.Printf(msg, rle.Reset, dur)
				time.Sleep(dur)
				continue // Retry request.
			} else {
				fmt.Printf("Problem parsing response: %v\n", err)
			}
		}
		batch := len(*results)
		if batch == 0 {
			fmt.Printf("No more results, end of timeline.\n")
			break
		}
		for _, tweet := range *results {
			if text, err = json.Marshal(tweet); err != nil {
				fmt.Printf("Could not encode Tweet: %v\n", err)
				os.Exit(1)
			}
			out.Write(text)
			out.Write([]byte("\n"))
      tweetIds = append(tweetIds, tweet.Id())
			max_id = tweet.Id() - 1
			total += 1
		}
		fmt.Printf("Got %v Tweets", batch)
		if resp.HasRateLimit() {
			fmt.Printf(", %v calls available", resp.RateLimitRemaining())
		}
		fmt.Printf(".\n")
	}
	fmt.Printf("--------------------------------------------------------\n")
	fmt.Printf("Wrote %v Tweets to %v\n", total, args.OutputFile)


  fmt.Println("\033[38:2:0:200:0mUnliking said tweets\033[0m")
  fmt.Println("\033[38:2:100:0:0m"+fmt.Sprint(len(tweetIds))+" first up is tweet, "+fmt.Sprint(tweetIds[0]))
  for i := range tweetIds {
    query = url.Values{}
    query.Set("id", fmt.Sprint(tweetIds[i]))
    endpoint := fmt.Sprintf(urlUnlike, query.Encode())
    fmt.Println("\033[38:2:100:100:0mDELETING "+fmt.Sprint(tweetIds[i])+"\033[0m")
    req, err = http.NewRequest("POST", endpoint, nil)
    if err != nil {
      panic(err)
    }
    resp, err = client.SendRequest(req)
    if err != nil {
      panic(err)
    }
    results = &twittergo.Timeline{}
    time.Sleep(100*time.Millisecond)

  }


}

